import express from 'express';
import bodyParser from 'body-parser';
import { MongoClient } from 'mongodb';
import path from 'path';

const app = express();

app.use(express.static(path.join(__dirname,'/build')));
app.use(bodyParser.json());

app.use(bodyParser.json());

app.get('/hello/:name', (req,res)=>{
    res.send(`Hello ${req.params.name}`);
});
app.post('/hello', (req,res)=>{
    console.log(req.body)
    res.send(`Hello ${req.body.name}`);
});

app.post('/api/articles/:name/upvote', async (req,res)=>{
    const articleName = req.params.name;
    try{
        const client = await MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true });
        const db = client.db('my-blog');
        const articleinfo = await db.collection('articles').findOne({ 'name': articleName });
        await db.collection('articles').updateOne({ name: articleName }, {
            $set: {
                upvotes: articleinfo.upvotes + 1,
            }
        });
        const updatedarticle = await db.collection('articles').findOne({ name: articleName });
        res.status(200).json(updatedarticle);
        client.close();
    } 
    catch (ex) {
        console.error(ex);
        res.status(500).json({ 'message': 'Error occurred', ex });
    }
});

app.post('/api/articles/:name/add-comments', async (req,res)=>{
    const articleName = req.params.name;
    const {username,text} = req.body;

    const client = await MongoClient.connect('mongodb://localhost:27017', { useNewUrlParser: true });
    const db = client.db('my-blog');
    const articleinfo = await db.collection('articles').findOne({ 'name': articleName });
    await db.collection('articles').updateOne({ name: articleName }, {
        $set: {
            comments: articleinfo.comments.concat({username,text})
        }
    });
    const updatedarticle = await db.collection('articles').findOne({ name: articleName });
    res.status(200).json(updatedarticle);
})

app.get('/api/articles/:name', async (req,res)=>{
    try{
        const articleName = req.params.name;
        const client = await MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser:true});
        console.log('connected to db');
        const db = client.db('my-blog');
        const articleinfo = await db.collection('articles').findOne({'name':articleName});
        res.status(200).json(articleinfo);
        client.close();
    }
    catch(ex){
        console.error(ex);
        res.status(500).json({'message':'Error occurred',ex});
    }
});

app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname + '/build/index.html'));
});

app.listen(8000, ()=>{
    console.log('Listening on port 8000');
});